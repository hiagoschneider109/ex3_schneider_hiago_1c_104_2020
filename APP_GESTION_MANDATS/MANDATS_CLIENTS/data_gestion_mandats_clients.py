# data_gestion_genres_films.py
# OM 2020.04.22 Permet de gérer (CRUD) les données de la table intermédiaire "t_genres_films"

from flask import flash
from APP_GESTION_MANDATS.DATABASE.connect_db_context_manager import MaBaseDeDonnee
from APP_GESTION_MANDATS.DATABASE.erreurs import *


class GestionMandatsClients():
    def __init__ (self):
        try:
            # DEBUG bon marché : Pour afficher un message dans la console.
            print("dans le try de gestions genres")
            # OM 2020.04.11 La connexion à la base de données est-elle active ?
            # Renvoie une erreur si la connexion est perdue.
            MaBaseDeDonnee().connexion_bd.ping(False)
        except Exception as erreur:
            flash("Dans Gestion genres films ...terrible erreur, il faut connecter une base de donnée", "danger")
            # DEBUG bon marché : Pour afficher un message dans la console.
            print(f"Exception grave Classe constructeur GestionMandatsClients {erreur.args[0]}")
            # Ainsi on peut avoir un message d'erreur personnalisé.
            raise MaBdErreurConnexion(f"{msg_erreurs['ErreurConnexionBD']['message']} {erreur.args[0]}")
        print("Classe constructeur GestionMandatsClients ")

    def mandats_afficher_data (self):
        try:
            # OM 2020.04.07 C'EST LA QUE VOUS ALLEZ DEVOIR PLACER VOTRE PROPRE LOGIQUE MySql
            # la commande MySql classique est "SELECT * FROM t_genres"
            # Pour "lever"(raise) une erreur s'il y a des erreurs sur les noms d'attributs dans la table
            # donc, je précise les champs à afficher
            strsql_genres_afficher = """SELECT id_Mandat, Description_Mandat FROM T_Mandat ORDER BY id_Mandat ASC"""
            # Du fait de l'utilisation des "context managers" on accède au curseur grâce au "with".
            with MaBaseDeDonnee().connexion_bd.cursor() as mc_afficher:
                # Envoi de la commande MySql
                mc_afficher.execute(strsql_genres_afficher)
                # Récupère les données de la requête.
                data_mandats = mc_afficher.fetchall()
                # Affichage dans la console
                print("data_mandats ", data_mandats, " Type : ", type(data_mandats))
                # Retourne les données du "SELECT"
                return data_mandats
        except pymysql.Error as erreur:
            print(f"DGG gad pymysql errror {erreur.args[0]} {erreur.args[1]}")
            raise MaBdErreurPyMySl(
                f"DGG gad pymysql errror {msg_erreurs['ErreurPyMySql']['message']} {erreur.args[0]} {erreur.args[1]}")
        except Exception as erreur:
            print(f"DGG gad Exception {erreur.args}")
            raise MaBdErreurConnexion(f"DGG gad Exception {msg_erreurs['ErreurConnexionBD']['message']} {erreur.args}")
        except pymysql.err.IntegrityError as erreur:
            # OM 2020.04.09 On dérive "pymysql.err.IntegrityError" dans le fichier "erreurs.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            raise MaBdErreurConnexion(f"DGG gad pei {msg_erreurs['ErreurConnexionBD']['message']} {erreur.args[1]}")

    def mandats_clients_afficher_data (self, valeur_id_client_selected_dict):
        print("valeur_id_client_selected_dict...", valeur_id_client_selected_dict)
        try:

            # OM 2020.04.07 C'EST LA QUE VOUS ALLEZ DEVOIR PLACER VOTRE PROPRE LOGIQUE MySql
            # la commande MySql classique est "SELECT * FROM t_genres"
            # Pour "lever"(raise) une erreur s'il y a des erreurs sur les noms d'attributs dans la table
            # donc, je précise les champs à afficher

            strsql_client_selected = """SELECT id_Mandat, Description_Mandat, EstimationTemps_Mandat, CoutEstime_Mandat GROUP_CONCAT(id_Employe) as MandatsClients FROM T_Client_Accepte_Mandat AS T1
                                        INNER JOIN T_Client AS T2 ON T2.id_Client = T1.FK_Client
                                        INNER JOIN T_Mandat AS T3 ON T3.id_Mandat = T1.FK_Mandat
                                        WHERE id_Client = %(value_id_client_selected)s"""

            strsql_genres_films_non_attribues = """SELECT id_Mandat, Description_Mandat, EstimationTemps_Mandat, CoutEstime_Mandat FROM T_Mandat
                                                    WHERE id_Mandat not in(SELECT id_Mandat as idMandatsClients FROM T_Client_Accepte_Mandat AS T1
                                                    INNER JOIN T_Client AS T2 ON T2.id_Client = T1.FK_Client
                                                    INNER JOIN T_Mandat AS T3 ON T3.id_Mandat = T1.FK_Mandat
                                                    WHERE id_Client = %(value_id_client_selected)s)"""

            strsql_mandats_clients_attribues = """SELECT id_Client, id_Mandat, Description_Mandat, EstimationTemps_Mandat, CoutEstime_Mandat  FROM T_Client_Accepte_Mandat AS T1
                                            INNER JOIN T_Client AS T2 ON T2.id_Client = T1.FK_Client
                                            INNER JOIN T_Mandat AS T3 ON T3.id_Mandat = T1.FK_Mandat
                                            WHERE id_Client = %(value_id_client_selected)s"""

            # Du fait de l'utilisation des "context managers" on accède au curseur grâce au "with".
            with MaBaseDeDonnee().connexion_bd.cursor() as mc_afficher:
                # Envoi de la commande MySql
                mc_afficher.execute(strsql_genres_films_non_attribues, valeur_id_client_selected_dict)
                # Récupère les données de la requête.
                data_mandats_clients_non_attribues = mc_afficher.fetchall()
                # Affichage dans la console
                print("dfad data_mandats_clients_non_attribues ", data_mandats_clients_non_attribues, " Type : ",
                      type(data_mandats_clients_non_attribues))

                # Envoi de la commande MySql
                mc_afficher.execute(strsql_client_selected, valeur_id_client_selected_dict)
                # Récupère les données de la requête.
                data_client_selected = mc_afficher.fetchall()
                # Affichage dans la console
                print("data_client_selected  ", data_client_selected, " Type : ", type(data_client_selected))

                # Envoi de la commande MySql
                mc_afficher.execute(strsql_mandats_clients_attribues, valeur_id_client_selected_dict)
                # Récupère les données de la requête.
                data_mandats_clients_attribues = mc_afficher.fetchall()
                # Affichage dans la console
                print("data_mandats_clients_attribues ", data_mandats_clients_attribues, " Type : ",
                      type(data_mandats_clients_attribues))

                # Retourne les données du "SELECT"
                return data_client_selected, data_mandats_clients_non_attribues, data_mandats_clients_attribues
        except pymysql.Error as erreur:
            print(f"DGGF gfad pymysql errror {erreur.args[0]} {erreur.args[1]}")
            raise MaBdErreurPyMySl(
                f"DGG gad pymysql errror {msg_erreurs['ErreurPyMySql']['message']} {erreur.args[0]} {erreur.args[1]}")
        except Exception as erreur:
            print(f"DGGF gfad Exception {erreur.args}")
            raise MaBdErreurConnexion(f"DGG gad Exception {msg_erreurs['ErreurConnexionBD']['message']} {erreur.args}")
        except pymysql.err.IntegrityError as erreur:
            # OM 2020.04.09 On dérive "pymysql.err.IntegrityError" dans le fichier "erreurs.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            raise MaBdErreurConnexion(f"DGGF gfad pei {msg_erreurs['ErreurConnexionBD']['message']} {erreur.args[1]}")

    def mandats_clients_afficher_data_concat (self, id_client_selected):
        print("id_client_selected  ", id_client_selected)
        try:
            # OM 2020.04.07 C'EST LA QUE VOUS ALLEZ DEVOIR PLACER VOTRE PROPRE LOGIQUE MySql
            # la commande MySql classique est "SELECT * FROM t_genres"
            # Pour "lever"(raise) une erreur s'il y a des erreurs sur les noms d'attributs dans la table
            # donc, je précise les champs à afficher

            strsql_mandats_clients_afficher_data_concat = """SELECT id_Client, Nom_Client, Prenom_Client,
                                                            GROUP_CONCAT(Description_Mandat) as MandatsClients FROM T_Client_Accepte_Mandat AS T1
                                                            RIGHT JOIN T_Client AS T2 ON T2.id_Client = T1.FK_CLient
                                                            LEFT JOIN T_Mandat AS T3 ON T3.id_Mandat = T1.FK_Mandat
                                                            GROUP BY id_Client"""

            # Du fait de l'utilisation des "context managers" on accède au curseur grâce au "with".
            with MaBaseDeDonnee().connexion_bd.cursor() as mc_afficher:
                # le paramètre 0 permet d'afficher tous les films
                # Sinon le paramètre représente la valeur de l'id du client
                if id_client_selected == 0:
                    mc_afficher.execute(strsql_mandats_clients_afficher_data_concat)
                else:
                    # Constitution d'un dictionnaire pour associer l'id du client sélectionné avec un nom de variable
                    valeur_id_client_selected_dictionnaire = {"value_id_client_selected": id_client_selected}
                    strsql_mandats_clients_afficher_data_concat += """ HAVING id_Client= %(value_id_client_selected)s"""
                    # Envoi de la commande MySql
                    mc_afficher.execute(strsql_mandats_clients_afficher_data_concat, valeur_id_client_selected_dictionnaire)

                # Récupère les données de la requête.
                data_mandats_clients_afficher_concat = mc_afficher.fetchall()
                # Affichage dans la console
                print("dggf data_mandats_clients_afficher_concat ", data_mandats_clients_afficher_concat, " Type : ",
                      type(data_mandats_clients_afficher_concat))

                # Retourne les données du "SELECT"
                return data_mandats_clients_afficher_concat


        except pymysql.Error as erreur:
            print(f"DGGF gfadc pymysql errror {erreur.args[0]} {erreur.args[1]}")
            raise MaBdErreurPyMySl(
                f"DGG gad pymysql errror {msg_erreurs['ErreurPyMySql']['message']} {erreur.args[0]} {erreur.args[1]}")
        except Exception as erreur:
            print(f"DGGF gfadc Exception {erreur.args}")
            raise MaBdErreurConnexion(
                f"DGG gfadc Exception {msg_erreurs['ErreurConnexionBD']['message']} {erreur.args}")
        except pymysql.err.IntegrityError as erreur:
            # OM 2020.04.09 On dérive "pymysql.err.IntegrityError" dans le fichier "erreurs.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            raise MaBdErreurConnexion(f"DGGF gfadc pei {msg_erreurs['ErreurConnexionBD']['message']} {erreur.args[1]}")

    def mandats_clients_add (self, valeurs_insertion_dictionnaire):
        try:
            print(valeurs_insertion_dictionnaire)
            # OM 2020.04.07 C'EST LA QUE VOUS ALLEZ DEVOIR PLACER VOTRE PROPRE LOGIQUE MySql
            # Insérer une (des) nouvelle(s) association(s) entre "id_film" et "id_genre" dans la "t_genre_film"
            strsql_insert_mandat_client = """INSERT INTO T_Client_Accepte_Mandat (id_Client_Accepte_Mandat, FK_Client, FK_Mandat)
                                            VALUES (NULL, %(value_fk_client)s, %(value_fk_mandat)s)"""

            # Du fait de l'utilisation des "context managers" on accède au curseur grâce au "with".
            # la subtilité consiste à avoir une méthode "mabd_execute" dans la classe "MaBaseDeDonnee"
            # ainsi quand elle aura terminé l'insertion des données le destructeur de la classe "MaBaseDeDonnee"
            # sera interprété, ainsi on fera automatiquement un commit
            with MaBaseDeDonnee() as mconn_bd:
                mconn_bd.mabd_execute(strsql_insert_mandat_client, valeurs_insertion_dictionnaire)


        except pymysql.err.IntegrityError as erreur:
            # OM 2020.04.09 On dérive "pymysql.err.IntegrityError" dans "MaBdErreurDoublon" fichier "erreurs.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            raise MaBdErreurDoublon(
                f"DGG pei erreur doublon {msg_erreurs['ErreurDoublonValue']['message']} et son status {msg_erreurs['ErreurDoublonValue']['status']}")

    def mandats_clients_delete (self, valeurs_insertion_dictionnaire):
        try:
            print(valeurs_insertion_dictionnaire)
            # OM 2020.04.07 C'EST LA QUE VOUS ALLEZ DEVOIR PLACER VOTRE PROPRE LOGIQUE MySql
            # Effacer une (des) association(s) existantes entre "id_film" et "id_genre" dans la "t_genre_film"
            strsql_delete_mandat_client = """DELETE FROM T_Client_Accepte_Mandat WHERE FK_Client = %(value_fk_client)s AND Fk_Mandat = %(value_fk_mandat)s"""

            # Du fait de l'utilisation des "context managers" on accède au curseur grâce au "with".
            # la subtilité consiste à avoir une méthode "mabd_execute" dans la classe "MaBaseDeDonnee"
            # ainsi quand elle aura terminé l'insertion des données le destructeur de la classe "MaBaseDeDonnee"
            # sera interprété, ainsi on fera automatiquement un commit
            with MaBaseDeDonnee() as mconn_bd:
                mconn_bd.mabd_execute(strsql_delete_mandat_client, valeurs_insertion_dictionnaire)
        except (Exception,
                pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                pymysql.IntegrityError,
                TypeError) as erreur:
            # DEBUG bon marché : Pour afficher un message dans la console.
            print(f"Problème mandats_clients Gestions Mandats Clients numéro de l'erreur : {erreur}")
            # C'est une erreur à signaler à l'utilisateur de cette application WEB.
            flash(f"Flash. Problème mandats_clients Gestions Mandats Clients  numéro de l'erreur : {erreur}", "danger")
            raise Exception(
                "Raise exception... Problème mandats_clients_delete Gestions Mandats Clients  {erreur}")

    def edit_mandat_data (self, valeur_id_dictionnaire):
        try:
            print(valeur_id_dictionnaire)
            # OM 2020.04.07 C'EST LA QUE VOUS ALLEZ DEVOIR PLACER VOTRE PROPRE LOGIQUE MySql
            # Commande MySql pour afficher le genre sélectionné dans le tableau dans le formulaire HTML
            str_sql_id_client = "SELECT id_Mandat, Description_Mandat FROM T_Mandat WHERE id_Mandat = %(value_id_mandats_selected)s"

            # Du fait de l'utilisation des "context managers" on accède au curseur grâce au "with".
            # la subtilité consiste à avoir une méthode "mabd_execute" dans la classe "MaBaseDeDonnee"
            # ainsi quand elle aura terminé l'insertion des données le destructeur de la classe "MaBaseDeDonnee"
            # sera interprété, ainsi on fera automatiquement un commit
            with MaBaseDeDonnee().connexion_bd as mconn_bd:
                with mconn_bd as mc_cur:
                    mc_cur.execute(str_sql_id_client, valeur_id_dictionnaire)
                    data_one = mc_cur.fetchall()
                    print("valeur_id_dictionnaire...", data_one)
                    return data_one

        except Exception as erreur:
            # OM 2020.03.01 Message en cas d'échec du bon déroulement des commandes ci-dessus.
            print(f"Problème edit_mandat_data Data Gestions Mandats numéro de l'erreur : {erreur}")
            # flash(f"Flash. Problèmes Data Gestions Genres numéro de l'erreur : {erreur}", "danger")
            # OM 2020.04.09 On dérive "Exception" par le "@obj_mon_application.errorhandler(404)" fichier "run_mon_app.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            raise Exception(
                "Raise exception... Problème edit_mandat_data d'un genre Data Gestions Genres {erreur}")

    def update_mandat_data (self, valeur_update_dictionnaire):
        try:
            print(valeur_update_dictionnaire)
            # OM 2019.04.02 Commande MySql pour la MODIFICATION de la valeur "CLAVIOTTEE" dans le champ "nameEditIntituleGenreHTML" du form HTML "GenresEdit.html"
            # le "%s" permet d'éviter des injections SQL "simples"
            # <td><input type = "text" name = "nameEditIntituleGenreHTML" value="{{ row.intitule_genre }}"/></td>
            str_sql_update_descriptionmandat = "UPDATE T_Mandat SET Description_Mandat = %(value_description_mandat)s WHERE id_Mandat = %(value_id_mandat)s"

            # Du fait de l'utilisation des "context managers" on accède au curseur grâce au "with".
            # la subtilité consiste à avoir une méthode "mabd_execute" dans la classe "MaBaseDeDonnee"
            # ainsi quand elle aura terminé l'insertion des données le destructeur de la classe "MaBaseDeDonnee"
            # sera interprété, ainsi on fera automatiquement un commit
            with MaBaseDeDonnee().connexion_bd as mconn_bd:
                with mconn_bd as mc_cur:
                    mc_cur.execute(str_sql_update_descriptionmandat, valeur_update_dictionnaire)

        except (Exception,
                pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                pymysql.IntegrityError,
                TypeError) as erreur:
            # OM 2020.03.01 Message en cas d'échec du bon déroulement des commandes ci-dessus.
            print(f"Problème update_mandat_data Data Gestions Genres numéro de l'erreur : {erreur}")
            # flash(f"Flash. Problèmes Data Gestions Genres numéro de l'erreur : {erreur}", "danger")
            # raise Exception('Raise exception... Problème update_mandat_data d\'un genre Data Gestions Genres {}'.format(str(erreur)))
            if erreur.args[0] == 1062:
                flash(f"Flash. Cette valeur existe déjà : {erreur}", "warning")
                # Deux façons de communiquer une erreur causée par l'insertion d'une valeur à double.
                flash(f"Doublon !!! Introduire une valeur différente", "warning")
                # Message en cas d'échec du bon déroulement des commandes ci-dessus.
                print(f"Problème update_mandat_data Data Gestions Genres numéro de l'erreur : {erreur}")

                raise Exception("Raise exception... Problème update_mandat_data d'un genre DataGestionsGenres {erreur}")

    def delete_select_mandat_data (self, valeur_delete_dictionnaire):
        try:
            print(valeur_delete_dictionnaire)
            # OM 2019.04.02 Commande MySql pour la MODIFICATION de la valeur "CLAVIOTTEE" dans le champ "nameEditIntituleGenreHTML" du form HTML "GenresEdit.html"
            # le "%s" permet d'éviter des injections SQL "simples"
            # <td><input type = "text" name = "nameEditIntituleGenreHTML" value="{{ row.intitule_genre }}"/></td>

            # OM 2020.04.07 C'EST LA QUE VOUS ALLEZ DEVOIR PLACER VOTRE PROPRE LOGIQUE MySql
            # Commande MySql pour afficher le genre sélectionné dans le tableau dans le formulaire HTML
            str_sql_select_id_mandat = "SELECT id_Mandat, Description_Mandat FROM T_Mandat WHERE id_Mandat = %(value_id_mandat)s"

            # Du fait de l'utilisation des "context managers" on accède au curseur grâce au "with".
            # la subtilité consiste à avoir une méthode"mabd_execute" dans la classe "MaBaseDeDonnee"
            # ainsi quand elle aura terminé l'insertion des données le destructeur de la classe "MaBaseDeDonnee"
            # sera interprété, ainsi on fera automatiquement un commit
            with MaBaseDeDonnee().connexion_bd as mconn_bd:
                with mconn_bd as mc_cur:
                    mc_cur.execute(str_sql_select_id_mandat, valeur_delete_dictionnaire)
                    data_one = mc_cur.fetchall()
                    print("valeur_id_dictionnaire...", data_one)
                    return data_one

        except (Exception,
                pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                pymysql.IntegrityError,
                TypeError) as erreur:
            # DEBUG bon marché : Pour afficher un message dans la console.
            print(f"Problème delete_select_mandat_data Gestions Mandats numéro de l'erreur : {erreur}")
            # C'est une erreur à signaler à l'utilisateur de cette application WEB.
            flash(f"Flash. Problème delete_select_mandat_data numéro de l'erreur : {erreur}", "danger")
            raise Exception(
                "Raise exception... Problème delete_select_mandat_data d\'un genre Data Gestions Genres {erreur}")

    def delete_mandat_data (self, valeur_delete_dictionnaire):
        try:
            print(valeur_delete_dictionnaire)
            # OM 2019.04.02 Commande MySql pour EFFACER la valeur sélectionnée par le "bouton" du form HTML "GenresEdit.html"
            # le "%s" permet d'éviter des injections SQL "simples"
            # <td><input type = "text" name = "nameEditIntituleGenreHTML" value="{{ row.intitule_genre }}"/></td>
            str_sql_delete_intitulefilm = "DELETE FROM T_Mandat WHERE id_Mandat = %(value_id_mandat)s"

            # Du fait de l'utilisation des "context managers" on accède au curseur grâce au "with".
            # la subtilité consiste à avoir une méthode "mabd_execute" dans la classe "MaBaseDeDonnee"
            # ainsi quand elle aura terminé l'insertion des données le destructeur de la classe "MaBaseDeDonnee"
            # sera interprété, ainsi on fera automatiquement un commit
            with MaBaseDeDonnee().connexion_bd as mconn_bd:
                with mconn_bd as mc_cur:
                    mc_cur.execute(str_sql_delete_intitulefilm, valeur_delete_dictionnaire)
                    data_one = mc_cur.fetchall()
                    print("valeur_id_dictionnaire...", data_one)
                    return data_one
        except (Exception,
                pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                pymysql.IntegrityError,
                TypeError) as erreur:
            # DEBUG bon marché : Pour afficher un message dans la console.
            print(f"Problème delete_mandat_data Data Gestions Mandats numéro de l'erreur : {erreur}")
            flash(f"Flash. Problèmes Data Gestions Mandats numéro de l'erreur : {erreur}", "danger")
            if erreur.args[0] == 1451:
                # OM 2020.04.09 Traitement spécifique de l'erreur 1451 Cannot delete or update a parent row: a foreign key constraint fails
                # en MySql le moteur INNODB empêche d'effacer un genre qui est associé à un client dans la table intermédiaire "t_genres_films"
                # il y a une contrainte sur les FK de la table intermédiaire "t_genres_films"
                # C'est une erreur à signaler à l'utilisateur de cette application WEB.
                flash(f"Flash. IMPOSSIBLE d'effacer !!! Ce Mandat est associé à des Clients dans la T_Client_Accepte_Mandat !!! : {erreur}", "danger")
                # DEBUG bon marché : Pour afficher un message dans la console.
                print(f"IMPOSSIBLE d'effacer !!! Ce Mandat est associé à des Clients dans la T_Client_Accepte_Mandat !!! : {erreur}")
            raise MaBdErreurDelete(f"DGG Exception {msg_erreurs['ErreurDeleteContrainte']['message']} {erreur}")