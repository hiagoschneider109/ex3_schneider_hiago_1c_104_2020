# Un objet "obj_mon_application" pour utiliser la classe Flask
# Pour les personnes qui veulent savoir ce que signifie __name__ une démonstration se trouve ici :
# https://www.studytonight.com/python/_name_-as-main-method-in-python
# __name__ garantit que la méthode run() est appelée uniquement lorsque main.py est exécuté en tant que programme principal.
# La méthode run() ne sera pas appelée si vous importez main.py dans un autre module Python.
from flask import Flask
from APP_GESTION_MANDATS.DATABASE import connect_db_context_manager


# Client qui fait "exister" notre application
obj_mon_application = Flask(__name__, template_folder="templates")
# Flask va pouvoir crypter les cookies
obj_mon_application.secret_key = '_vogonAmiral_)?^'

# Doit se trouver ici... soit après l'instanciation de la classe "Flask"
# OM 2020.03.25 Tout commence ici par "indiquer" les routes de l'application.
from APP_GESTION_MANDATS import routes
from APP_GESTION_MANDATS.ADRESSES import routes_gestion_adresses
from APP_GESTION_MANDATS.TELEPHONES_CLIENTS import routes_gestion_genres_films
from APP_GESTION_MANDATS.EMPLOYES import routes_gestion_employes
from APP_GESTION_MANDATS.MANDATS import routes_gestion_mandats
from APP_GESTION_MANDATS.CLIENTS import routes_gestion_films
from APP_GESTION_MANDATS.TELEPHONES import routes_gestion_genres
from APP_GESTION_MANDATS.ADRESSES_CLIENTS import routes_gestion_adresses_clients
from APP_GESTION_MANDATS.MANDATS_CLIENTS import routes_gestion_mandats_clients


